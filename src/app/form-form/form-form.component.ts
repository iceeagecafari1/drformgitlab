import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-form-form',
  templateUrl: './form-form.component.html',
  styleUrls: ['./form-form.component.css']
})
export class FormFormComponent implements OnInit {

  //user info
  user_id = localStorage.getItem('id');
  user_username = localStorage.getItem('user_name');
  user_name = localStorage.getItem('name');
  user_email = localStorage.getItem('email');
  user_position = localStorage.getItem('position');
  user_tel = localStorage.getItem('tel');
  user_roleid = localStorage.getItem('role_id');
  user_image_url = localStorage.getItem('image_url');
  user_image_type = localStorage.getItem('image_type');
  token_id = localStorage.getItem('token');
  url_ip:any;
  //form info
  form_id = localStorage.getItem('form_id');

  constructor() { }

  ngOnInit() {
    this.url_ip = localStorage.getItem('ip');
    console.log("User = "+this.user_name);
    console.log("Token = "+this.token_id);
  }

}
