import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginFormComponent } from './login-form/login-form.component';
import { FormsModule } from '@angular/forms';
import { MainFormComponent } from './main-form/main-form.component';
import { CreateformFormComponent } from './createform-form/createform-form.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { UserFormComponent } from './user-form/user-form.component';
import { HttpClientModule }    from '@angular/common/http';
import { FormFormComponent } from './form-form/form-form.component';



@NgModule({
  declarations: [
    AppComponent,
    LoginFormComponent,
    MainFormComponent,
    CreateformFormComponent,
    UserFormComponent,
    FormFormComponent,
  
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    NgbModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
