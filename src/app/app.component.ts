import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Drform1';
  ngOnInit() {
    var url_ip = "http://192.168.1.152:3000";
    localStorage.setItem('ip', url_ip);
  }
}
