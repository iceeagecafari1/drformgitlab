import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {

  url_ip:any;

  //login
  username: any = "";
  password: any = "";

  //register
  user_form: any = "";
  pass_form: any = "";
  confirmPass_form: any = "";
  name_form: any = "";
  email_form: any = "";
  phone_form: any = "";
  position_form: any = "";

  //alert message
  alertMeg: String = "";

  //image form
  formImage = [
    {
      name: "user.png",
      path: "/assets/img/user.png", 
      type: "base64"
    }
  ];
  imageName: any;
  selectedFiles: File;

  showRegister:boolean = false;

  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.url_ip = localStorage.getItem('ip');
    console.log(this.url_ip);
  }

  login(){
    if(this.username == "" || this.password == ""){
      this.alertMeg = "username or password is empty.";
    }else{
      var login = {
        session:{
          username: this.username,
          password: this.password
        }
      }

      this.http.post(this.url_ip+"/login",login).subscribe((data)=>{
        if(data['message'] != "Invalid username or password"){



          localStorage.setItem('id', data['user_info']['id']);
          localStorage.setItem('user_name', data['user_info']['username']);
          localStorage.setItem('name', data['user_info']['name']);
          localStorage.setItem('email', data['user_info']['email']);
          localStorage.setItem('position', data['user_info']['position']);
          localStorage.setItem('tel', data['user_info']['tel']);
          localStorage.setItem('role_id', data['user_info']['role_id']);
          localStorage.setItem('image_url', data['user_info']['image'].url);
          localStorage.setItem('image_type', data['user_info']['image'].content_type);
          localStorage.setItem('token', data['token']);

          this.alertMeg = "";
          window.location.href = '/mains';
        }else{
          this.alertMeg = "Invalid username or password";
        }
          

      });

    }
  }

  createAccount(){
    if(this.user_form == "" || this.pass_form == "" || this.confirmPass_form == "" || this.email_form == "" || this.phone_form == "" || this.position_form == ""){
      this.alertMeg = "some text is empty."
    }
    else if(this.pass_form != this.confirmPass_form){
      this.alertMeg = "password is not match."
    }else{
      //console.log("user="+this.user_form+"\npass="+this.pass_form+"\nconfirmpass="+this.confirmPass_form+"\nemail="+this.email_form+"\nphone="+this.phone_form+"\nposition="+this.position_form);

      var imageText: any;
      let itemArray: any;

      for (let item of this.formImage){
        if(item.type === "image/jpeg"){
          imageText = item.path.split(",")[1];
        }
        else if(item.type === "image/png"){
          imageText = item.path.split(",")[1];
        }
        else if(item.type === "video/mp4"){
          imageText = item.path.split(",")[1];
        }
        else{ //null
          imageText = "";
        }
  
        itemArray = imageText;
      }



      var register = {
        user:{
          username: this.user_form,
          password: this.pass_form,
          name: this.name_form,
          email: this.email_form,
          tel: this.phone_form,
          position: this.position_form,
          image: itemArray
        }
      }

      this.http.post(this.url_ip+"/users",register,{
        headers: new HttpHeaders({
          'type': "base64",
        })
      }).subscribe((data)=>{
        console.log(data);
      });


    }
  }

  showPage(){
    this.showRegister =! this.showRegister;
    this.alertMeg = "";
  }

  handleFileInput(event){
    this.formImage = [];
    for (let i = 0; i < event.target.files.length; i++) {
      this.selectedFiles = <File>event.target.files[i];

      var reader = new FileReader();
      reader.onload = (event:any) => {
        this.formImage.push({
          name: this.selectedFiles.name,
          path: event.target.result,
          type: this.selectedFiles.type
        });
        this.imageName = this.selectedFiles.name
      }
      reader.readAsDataURL(this.selectedFiles);
      console.log(this.imageName);
    }
  }


}
