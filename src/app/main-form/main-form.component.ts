import { Component, OnInit} from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CreateformFormComponent } from '../createform-form/createform-form.component';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { getLocaleDateFormat } from '@angular/common';

@Component({
  selector: 'app-main-form',
  templateUrl: './main-form.component.html',
  styleUrls: ['./main-form.component.css']
})
export class MainFormComponent implements OnInit {

  //user info
  user_id = localStorage.getItem('id');
  user_username = localStorage.getItem('user_name');
  user_name = localStorage.getItem('name');
  user_email = localStorage.getItem('email');
  user_position = localStorage.getItem('position');
  user_tel = localStorage.getItem('tel');
  user_roleid = localStorage.getItem('role_id');
  user_image_url = localStorage.getItem('image_url');
  user_image_type = localStorage.getItem('image_type');
  token_id = localStorage.getItem('token');
  url_ip:any;

  //link Nav bars
  isMyform: boolean = true;
  isShareform: boolean = false;
  isUserform: boolean = false;
  isNewform: boolean = false;
  isForm: boolean = false;

  //picture
  imageUrl = [
    {
      path: "/assets/img/picture.png",
     type:"image/png"
    }
  ];

  //myform
  myForm = [];

  constructor(private modalService: NgbModal,private http: HttpClient) { }

  ngOnInit() {
    this.url_ip = localStorage.getItem('ip');
    console.log("User = "+this.user_username);
    console.log("Token = "+this.token_id);

    console.log(this.user_image_url);


    this.getForm();

  }

  selectMyform(){
    this.isMyform = true;
    this.isShareform = false;
    this.isUserform = false;
    this.isNewform = false;
    this.isForm = false;
  }
  selectShareform(){
    this.isShareform = true;
    this.isMyform = false;
    this.isUserform = false;
    this.isNewform = false;
    this.isForm = false;
  }
  selectUserform(){
    this.isUserform = true;
    this.isShareform = false;
    this.isMyform = false;
    this.isNewform = false;
    this.isForm = false;
  }

  newForm(content) {
    //const modalRef = this.modalService.open(CreateformFormComponent);
    this.isNewform = true;
    this.isShareform = false;
    this.isMyform = false;
    this.isUserform = false;
    this.isForm = false;
  }

  selectForm(index){
    localStorage.setItem('form_id', index);
    this.isForm = true;
    this.isNewform = false;
    this.isShareform = false;
    this.isMyform = false;
    this.isUserform = false;
  }

  logOut(){
    this.http.delete(this.url_ip+"/logout",{
      headers: new HttpHeaders({
        'token': this.token_id,
      })
    }).subscribe((data)=>{
      console.log(data);

      localStorage.setItem('id', "");
      localStorage.setItem('user_name', "");
      localStorage.setItem('name', "");
      localStorage.setItem('email', "");
      localStorage.setItem('position', "");
      localStorage.setItem('tel', "");
      localStorage.setItem('role_id', "");
      localStorage.setItem('image_url', "");
      localStorage.setItem('image_type', "");
      localStorage.setItem('token', "");

      window.location.href = '/logins';

  
    });
  }

  getForm(){
    this.http.get(this.url_ip + '/forms',{
      headers: new HttpHeaders({
        'token': this.token_id,
      })
    }).subscribe((data)=>{
      console.log(data);
      var keys = Object.keys(data);
      var len = keys.length;

      if(len > 0){
        for (let i = 0; i < len; i++) {
          this.myForm.push({
            id: data[i]['id'],
            title: data[i]['title'],
            description: data[i]['description'],
            user_id: data[i]['user_id'],
            image:[
              {
                url: data[i]['image'].url,
                content_type: data[i]['image'].content_type
              }
            ],
            item: data[i]['item'],
            share: data[i]['share'],
          })   
        }
        console.log(this.myForm);
      }
      
    });
  }



}
