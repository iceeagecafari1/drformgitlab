import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateformFormComponent } from './createform-form.component';

describe('CreateformFormComponent', () => {
  let component: CreateformFormComponent;
  let fixture: ComponentFixture<CreateformFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateformFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateformFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
