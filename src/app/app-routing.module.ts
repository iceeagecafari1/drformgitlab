import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginFormComponent } from './login-form/login-form.component';
import { MainFormComponent } from './main-form/main-form.component';
import { CreateformFormComponent } from './createform-form/createform-form.component';
import { UserFormComponent } from './user-form/user-form.component';
import { FormFormComponent } from './form-form/form-form.component';



const routes: Routes = [
  { path: '', redirectTo: '/logins', pathMatch: 'full' },
  { path: 'logins', component: LoginFormComponent },
  { path: 'mains', component: MainFormComponent },
  { path: 'createforms', component: CreateformFormComponent},
  { path: 'users', component: UserFormComponent },
  { path: 'forms/:id', component: FormFormComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
